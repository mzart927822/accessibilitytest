import cv2
import utils
import numpy as np
from sklearn.cluster import KMeans



rawImage = 'C:/Users/asim.tarar/git/accessibilitytest/rawImages/image1.png'

imageToDetect = cv2.imread(rawImage)

originalHight, originalWidth, channels = imageToDetect.shape
print("Original Picture in Color: Rows x Columns of the image are {} and {} with {} channels.".format(originalHight, originalWidth, channels))
print ""
print("Original Picture in  Color: Total pixels are {}.".format(imageToDetect.size))
print ""

print("Original Picture in Color: The image data type is {}.".format(imageToDetect.dtype))
print ""


cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL) 
selectClosedCaptionArea = cv2.selectROI("Original Image", imageToDetect)
closedCaptionArea = imageToDetect[int(selectClosedCaptionArea[1]):int(selectClosedCaptionArea[1]+selectClosedCaptionArea[3]), int(selectClosedCaptionArea[0]):int(selectClosedCaptionArea[0]+selectClosedCaptionArea[2])]
cv2.namedWindow("Closed Caption Area", cv2.WINDOW_NORMAL)
cv2.imshow("Closed Caption Area",closedCaptionArea)
cv2.waitKey()

reShapedImage = closedCaptionArea.reshape((closedCaptionArea.shape[0] * closedCaptionArea.shape[1], 3))
clt = KMeans(n_clusters = 2)
clt.fit(reShapedImage)



magenta = ([153,0,153],[255,128,255])
black = ([0,0,0],[38,38,38])
white = ([204,204,204],[255,255,255])
yellow = ([0,153,153],[102,255,255])
red = ([0,0,153],[51,51,255])
blue = ([26,0,0],[255,153,153])
green = ([0,26,0],[153,255,153])
cyan = ([153,153,0],[255,255,153])

colors = [magenta, black, white, yellow, red, blue, green, cyan]

# for (lower, upper) in colors:
#     lower = np.array(lower, dtype = "uint8")
#     upper = np.array(upper, dtype = "uint8")
#     mask = cv2.inRange(closedCaptionArea, lower, upper)
#     result = cv2.bitwise_and(closedCaptionArea,closedCaptionArea, mask= mask)
#     cv2.imshow("i", result)
#     cv2.waitKey(0)


    