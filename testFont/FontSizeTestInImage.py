import cv2
import numpy as np
from cmath import sqrt



# rawImage with image path
rawImage = 'C:/Users/asim.tarar/git/accessibilitytest/rawImages/image2.png'

# read the image and assign it to imageToDetect. 
# This will read the image in color. 0 for gray scale, 1 for color, -1 for unchanged
# images in OpenCV are read BGR not RGB
imageToDetect = cv2.imread(rawImage)

# print the shape of the image, rows x columns and channels
originalHight, originalWidth, channels = imageToDetect.shape
print("Original Picture in Color: Rows x Columns of the image are {} and {} with {} channels.".format(originalHight, originalWidth, channels))
print ""
# print the total pixels in the image, rows x columns x length
print("Original Picture in  Color: Total pixels are {}.".format(imageToDetect.size))
print ""
# print the image data type
print("Original Picture in Color: The image data type is {}.".format(imageToDetect.dtype))
print ""

# Get the PPI assuming  a 55 inch TV
diagonalPixels = sqrt((originalHight**2) + (originalWidth**2)) 
PPITotal = diagonalPixels / 55
PPI = abs(PPITotal)

# first window is created to fit large size image in sve.selectROI
# select the closed caption area. The image will show in a window named 'original image'.
# the image to select the region from is imageToDetect.
cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL) 
selectClosedCaptionArea = cv2.selectROI("Original Image", imageToDetect)
closedCaptionArea = imageToDetect[int(selectClosedCaptionArea[1]):int(selectClosedCaptionArea[1]+selectClosedCaptionArea[3]), int(selectClosedCaptionArea[0]):int(selectClosedCaptionArea[0]+selectClosedCaptionArea[2])]
cv2.namedWindow("Closed Caption Area", cv2.WINDOW_NORMAL)
cv2.imshow("Closed Caption Area",closedCaptionArea)
cv2.waitKey()


# convert the image to gray scale
imageToGray = cv2.cvtColor(closedCaptionArea, cv2.COLOR_BGR2GRAY)

# print the shape of the image, rows x columns and length of each axis
print("closedCaptionArea image to gray: Rows x Columns of the image are {}".format(imageToGray.shape))
print ""
# print the total pixels in the image, rows x columns x length
print("closedCaptionArea image to gray: Total pixels are  {}".format(imageToGray.size))
print ""
# print the image data type
print("closedCaptionArea image to gray: The image data type is {}.".format(imageToGray.dtype))
print ""

retVal, mask = cv2.threshold(imageToGray, 180, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

image_final = cv2.bitwise_and(imageToGray, imageToGray, mask=mask)
# 
retVal, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)
# 
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
dilated = cv2.dilate(new_img, kernel, iterations=9)
# 
image, contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
# 


index = 1
totalPoints = 0
for contour in contours:
    [x, y, w, h] = cv2.boundingRect(contour)
    if w < 35 and h < 35:
        continue
    # draw a rectangle around each contour. In color blue with thickness of 3 
    cv2.rectangle(closedCaptionArea, (x, y), (x + w, y + h), (255, 0, 0), 3) 
    if contour.size < 250:
        eachLetter = closedCaptionArea[y :y +  h , x : x + w]
        letterToGray = cv2.cvtColor(eachLetter, cv2.COLOR_BGR2GRAY)
        n_white_pix = np.sum(letterToGray == 255)
        if n_white_pix > 0:
            print('Shape of the image {} is {} with area of {}.'.format(index,letterToGray.shape,letterToGray.size))
            [hight, width] = letterToGray.shape
            points = hight *  .72 
            totalPoints += points
            letters = 'D:/images' + '/eachLetter_' + str(index) + '.png' 
            cv2.imwrite(letters , eachLetter)
            index = index + 1
print ""
averagePoints = totalPoints / index -1
print("The font point is {}".format(averagePoints))

 
# path to save image, what image to save           
cv2.imwrite('D:/images/imageToExamine.png', imageToDetect)
cv2.imwrite('D:/images/closedCaptionArea.png', closedCaptionArea)







